# Commands

{% hint style="info" %}
**Good to know:** your product docs aren't just a reference of all your features! use them to encourage folks to perform certain actions and discover the value in your product.
{% endhint %}

## The basics

Commands are the things you will be using to interact with Ayumi. If you're unsure about what the commands are, or what they do, simply run \*%help\* in a DM or channel, and Ayumi will DM you her commands.&#x20;

## Creating a project

Hit the big '+' button in your sidebar and select 'New Project' from the menu that pops up. Give your project a name, and you're good to go!
